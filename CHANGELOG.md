# Changelog
All notable changes to the Vuforia Engine package will be documented in this file.
To view detailed release notes, go to: https://library.vuforia.com/articles/Release_Notes.html

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [8.3.10] - 2019-07-19

Update package to Vuforia Engine version 8.3
